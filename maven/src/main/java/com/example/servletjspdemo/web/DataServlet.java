package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String selectedHobby = "";
		for (String hobby : request.getParameterValues("hobby")) {
			selectedHobby += hobby + " ";
		}
		
		String selectedFood = "";
		for (String food : request.getParameterValues("food")) {
			selectedFood += food + " ";
		}
		
		out.println("<html><body><h2>Your data</h2>" +
				"<p>Login: " + request.getParameter("login") + "<br />" +
				"<p>Password: " + request.getParameter("password") + "<br />" +
				"<p>Sex: " + request.getParameter("sex") + "<br />" +
				"<p>Car: " + request.getParameter("car") + "<br />" +
				"<p>Your hobby: " + selectedHobby + "<br />" +
				"<p>Your food: " + selectedFood + "<br />" +
				"</body></html>");
		out.close();
	}

}
